﻿using DataLayer.Constant;
using HtmlAgilityPack;
using Repository.Dtos;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Wheretoget_Webapi.Controllers
{
    //[Authorize]
    public class PostController : ApiController
    {
		private readonly IPostRepository _repo;
		public PostController(IPostRepository repo)
		{
			this._repo=repo;
		}
        public async Task<ApiResponse> CreatePostAndResource()
        {
            ApiResponse result=null;
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                result = new ApiResponse(HttpStatusCode.UnsupportedMediaType, "Unsupported mediaType");
            }
            var prefixList = new[] { "jpg", "jpeg", "png", "bmp", "gif" };
            string root = HttpContext.Current.Server.MapPath("~/Resources");
            var provider = new MultipartFormDataStreamProvider(root);

            try
            {
                // Read the form data.
                await Request.Content.ReadAsMultipartAsync(provider);

                //image mimetype control with file
                if (prefixList.Any(r => provider.FileData[0].Headers.ContentType.ToString().EndsWith(r)))
                {
                // This illustrates how to get the file names.
                    foreach (MultipartFileData file in provider.FileData)
                    {
                        CreatePostDto post = new CreatePostDto
                        {
                            Name = provider.FileData[0].Headers.ContentDisposition.FileName,
                            Path = file.LocalFileName,
                            Description = provider.FormData["description"],
                            Title = provider.FormData["title"],
                            MimeType = provider.FileData[0].Headers.ContentType.ToString(),
                            AuthorId = Guid.Parse("4DD72FD1-2324-E611-9C1B-9439E59F5D00") //Change to row

                        };
                        await _repo.CreatePostAndReesource(post);

                    }

                    result = new ApiResponse(HttpStatusCode.Accepted, "Resource create successfull");
                }
            }
            catch (Exception e)
            {
                result = new ApiResponse(HttpStatusCode.InternalServerError, e.Message);
            }
            return result;
        }

       
    }
}
