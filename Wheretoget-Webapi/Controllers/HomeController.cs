﻿using System.Linq;
using System.Web.Mvc;
using DataLayer.Context;

namespace Wheretoget_Webapi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            //var db = new WheretogetContext();
            //var user = db.User.ToList();
            return View();
        }
    }
}