﻿using DataLayer.Constant;
using Repository.Dtos;
using Repository.Interfaces;
using System.Threading.Tasks;
using System.Web.Http;

namespace Wheretoget_Webapi.Controllers
{
    [Authorize]
    public class CategoryController : ApiController
    {
        private readonly ICategoryRepository _repo ;
        public CategoryController(ICategoryRepository repo)
        {
            this._repo = repo;
        }
        // GET: Category
        [HttpPost]
        public async Task<ApiResponse> CreateCategory([FromBody]CreateCategoryDto category)
        {
            return await _repo.CreateCategory(category);
        }
        [HttpPost]
        public async Task<ApiResponse> CreateSubCategoryAndLevels([FromBody]CreateSubCategoryDto categories)
        {
            return await _repo.CreateSubCategoryAndLevels(categories);
        }
    }
}