﻿using DataLayer.Constant;
using Repository.Dtos;
using Repository.Interfaces;
using System.Threading.Tasks;
using System.Web.Http;

namespace Wheretoget_Webapi.Controllers
{
    [Authorize]
    public class CommentController : ApiController
    {
		private readonly ICommentRepository _repo ;
		public CommentController(ICommentRepository repo)
		{
			this._repo = repo;
		}
		[HttpPost]
		public async Task<ApiResponse> CreateComment([FromBody]CreateCommentDto comment)
		{
			return await _repo.CreateComment(comment);
		}
    }
}
