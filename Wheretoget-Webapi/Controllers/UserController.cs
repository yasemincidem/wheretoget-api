﻿using DataLayer.Constant;
using Repository.Dtos;
using Repository.Interfaces;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Wheretoget_Webapi.Controllers
{
    [AllowAnonymous]
	public class UserController : ApiController
    {
        private readonly IUserRepository _repo;
        public UserController(IUserRepository repo)
        {
            this._repo = repo;
        }
        [HttpPost]
        public async Task<ApiResponse> CreateUser([FromBody]CreateAndUpdateUserDto user)
        {
            return await _repo.CreateUser(user);
        }
        [HttpPost]
        public async Task<ApiResponse> UpdateUser([FromBody]CreateAndUpdateUserDto userprofile)
        {
            return await _repo.UpdateUser(userprofile);
        }
        [HttpPost]
        public async Task<ApiResponse> DeleteUser([FromBody]string emailAddress)
        {
            return await _repo.DeleteUser(emailAddress);
        }
		[HttpPost]
		public async Task<bool> CheckUserIsExists(string email,string password)
		{
			return await _repo.CheckUserIsExists(email,password);
		}

	}
}
