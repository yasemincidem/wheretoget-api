﻿
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using DataLayer.Constant;
using DataLayer.Models;
using HtmlAgilityPack;
using Repository.Dtos;
using Repository.Interfaces;

namespace Wheretoget_Webapi.Controllers
{
    public class TipController : ApiController
    {
        private readonly ITipRepository _repo=null;

        public TipController(ITipRepository repo)
        {
            _repo = repo;
        }

        public async Task<ApiResponse> CreateTip(CreateTipDto tipData)
        {
            return await _repo.CreateTip(tipData);
        }


        /// <summary>
        /// Get Images from external Url
        /// </summary>
        /// <param>
        ///     <name></name>
        /// </param>
        /// <returns />
        public async Task<ReturnTipDto> GetTips()
        {
            Tips tip = await _repo.GetTip();

            var images = new List<string>();
            var prefixList = new[] { "jpg", "jpeg", "png", "bmp", "gif" };

            var document = new HtmlWeb().Load(tip.Url);

            var imageNodes = document.DocumentNode.Descendants("img");

            var imageLinks = imageNodes.Where(r => r.Attributes.Contains("src"))
                                       .Select(r => r.Attributes["src"].Value);

            images.AddRange(imageLinks.Where(link => prefixList.Any(link.EndsWith)));

            var returnTip = new ReturnTipDto()
            {
                Images = images,
                AuthorId = tip.AuthorId,
                Price = tip.Price,
                Url=tip.Url,
                Description = tip.Description,
                PostId = tip.PostId,
                Like = tip.Likes
            };

            return returnTip;
        }

    }
}
