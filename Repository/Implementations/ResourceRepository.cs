﻿
using DataLayer.Constant;
using DataLayer.Context;
using DataLayer.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Repository.Implementations
{
   public  class ResourceRepository
    {
        private readonly WheretogetContext _db;
        public ResourceRepository(WheretogetContext db)
        {
            _db = db;
        }

       public async Task<Resources> GetResources(string path)
       {
           var result = await _db.Resource.Where(r => r.Path == path).FirstOrDefaultAsync();

           return result;
       }

       public async Task<ApiResponse> CreateResource(Resources newResource)
        {
            ApiResponse result;
            bool isExistsResource = await _db.Resource.AnyAsync(r => r.Path == newResource.Path);

            if (!isExistsResource)
            {
                Resources resource = new Resources()
                {
                    Path = newResource.Path,
                    Name = newResource.Name,
                    MimeType = newResource.MimeType,
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now
                };
                _db.Entry(resource).State = EntityState.Added;

                try
                {
                    await _db.SaveChangesAsync();
                    result = new ApiResponse(System.Net.HttpStatusCode.Accepted, "Resource created by successfull", resource);
                }
                catch (Exception ex)
                {
                    result = new ApiResponse(System.Net.HttpStatusCode.NotImplemented, ex.Message, resource);
                }
            }
            else
            {
                result = new ApiResponse(System.Net.HttpStatusCode.NotModified, "Resource already exists database");
            }

            return result;

        }

    }
}
