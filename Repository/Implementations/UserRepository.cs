﻿using DataLayer.Context;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Repository.Dtos;
using DataLayer.Constant;
using DataLayer.Models;
using Repository.Interfaces;

namespace Repository.Implementations
{
    public class UserRepository:IUserRepository
    {
        private readonly WheretogetContext _db;

        public UserRepository(WheretogetContext db)
        {
            this._db = db;
        }

        public async Task<Users> GetUser(Guid userId)
        {

            Users user = await _db.User.Where(r => r.UserId == userId).FirstOrDefaultAsync();

            return user;
        }
        public async Task<ApiResponse> CreateUser(CreateAndUpdateUserDto newUser)
        {
            ApiResponse result;

            bool isUserExists = await _db.User.AnyAsync(r => r.EmailAddress == newUser.Email);

            if (!isUserExists)
            {
                var user = new Users()
                {
                    UserId = Guid.NewGuid(),
                    Username = newUser.UserName,
                    Avatar = newUser.Avatar,
                    EmailAddress = newUser.Email,
                    Gender = (Enums.GenderEnum) newUser.Gender,
                    Location = newUser.Location,
                    Password = newUser.Password,
                    CreateDate = newUser.CreateDate,
                    UpdateDate = newUser.UpdateDate
                };

                _db.Entry(user).State = EntityState.Added;

                try
                {
                    await _db.SaveChangesAsync();
                    result = new ApiResponse(HttpStatusCode.Accepted, "User created by successfully", user);
                }
                catch (Exception ex)
                {
                    result = new ApiResponse(HttpStatusCode.NotModified, ex.Message, user);
                }
            }
            else
            {
                result=new ApiResponse(HttpStatusCode.NotFound, $"User not found");
            }

            return result;
        }

        public async Task<ApiResponse> UpdateUser(CreateAndUpdateUserDto newUser)
        {
            ApiResponse result;

            Users user = await _db.User.Where(r => r.UserId == newUser.UserId).FirstOrDefaultAsync();

            if (user != null)
            {

                user.EmailAddress = newUser.Email ?? null;
                user.Location = newUser.Location ?? null;
                user.Avatar = newUser.Avatar ?? null;
                user.Likes = newUser.Likes;
                user.Gender = (Enums.GenderEnum?) newUser.Gender;
                user.Password = newUser.Password;
                user.CreateDate = newUser.CreateDate;
                user.UpdateDate = newUser.UpdateDate;

                _db.Entry(user).State = EntityState.Modified;

                try
                {
                    result = new ApiResponse(HttpStatusCode.Accepted, "User modifies by successfully", user);
                }
                catch (Exception ex)
                {
                    result = new ApiResponse(HttpStatusCode.NotModified, ex.Message, user);
                }

            }
            else
            {
                result=new ApiResponse(HttpStatusCode.NotFound,"User not found");
            }

            return result;
        }

        public async Task<ApiResponse> DeleteUser(string userId)
        {
            ApiResponse result;

            Users user = await _db.User.Where(r => r.UserId.ToString() == userId).FirstOrDefaultAsync();

            if (user != null)
            {

                _db.User.Remove(user);

                _db.Entry(user).State = EntityState.Deleted;
                try
                {
                    await _db.SaveChangesAsync();

                    result = new ApiResponse(HttpStatusCode.Accepted, "User deleted by successfully", user);
                }
                catch (Exception ex)
                {
                    result = new ApiResponse(HttpStatusCode.NotModified, ex.Message, user);
                }

            }
            else
            {
                result=new ApiResponse(HttpStatusCode.NotFound,"User not found");
            }

            return result;
        }

        public async Task<bool> CheckUserIsExists(string email,string password)
        {

            bool isUserExists = await _db.User.AnyAsync(r => r.EmailAddress == email && r.Password == password);

            if (isUserExists)
            {
                return true;
            }

            return false;
        }

    }

}
