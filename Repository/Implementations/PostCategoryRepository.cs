﻿

using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DataLayer.Constant;
using DataLayer.Context;
using DataLayer.Models;
using Repository.Interfaces;

namespace Repository.Implementations
{
    public class PostCategoryRepository:IPostCategoryRepository
    {
        private readonly WheretogetContext _db;
        private readonly CategoryRepository _category;

        public PostCategoryRepository(WheretogetContext db)
        {
            _db = db;
            _category = new CategoryRepository(db);
        }

        public async Task<ApiResponse> CreatePostCategory(Guid postId,string categoryName)
        {
            ApiResponse result=null;

            bool isPostExists = await _db.Post.AnyAsync(r => r.Id == postId);
            bool isCategoryExists = await _db.Catgegory.AnyAsync(r => r.Name == categoryName);

            if (isCategoryExists && isPostExists)
            {
                Categories category = await _category.GetDistinctCategories(categoryName);
                PostCategories postCategories = new PostCategories()
                {
                    PostId = postId,
                    CategoryId = category.Id,
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now
                };
                _db.Entry(postCategories).State=EntityState.Added;
                try
                {
                    await _db.SaveChangesAsync();
                    result=new ApiResponse(HttpStatusCode.Accepted,"PostAnd Category relation create by successfull",postCategories);

                }
                catch (Exception ex)
                {
                   result=new ApiResponse(HttpStatusCode.NotImplemented,ex.Message);
                }
            }
            return result;

        }
    }
}
