﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DataLayer.Constant;
using DataLayer.Context;
using DataLayer.Models;
using Repository.Dtos;
using Repository.Interfaces;

namespace Repository.Implementations
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly WheretogetContext _db;

        public CategoryRepository(WheretogetContext db)
        {
            _db = db;
        }

        public async Task<Categories> GetDistinctCategories(string name)
        {
            Categories categories = await _db.Catgegory.Where(r => r.Name == name).Distinct().FirstOrDefaultAsync();

            return categories;
        }

        public async Task<ApiResponse> CreateCategory(CreateCategoryDto c)
        {
            ApiResponse result;

            bool isCategoryExists = await _db.Catgegory.AnyAsync(r => r.Name == c.Name);

            if (!isCategoryExists)
            {
                var category = new Categories
                {
                    Name = c.Name,
                    Levels = c.Levels,
                    CreateDate = c.CreateDate,
                    UpdateDate = c.UpdateDate
                };

                _db.Entry(category).State = EntityState.Added;

                try
                {
                    await _db.SaveChangesAsync();
                    result = new ApiResponse(HttpStatusCode.OK, "Success", category);
                }
                catch (Exception ex)
                {
                    result = new ApiResponse(HttpStatusCode.NotImplemented, ex.Message, category);
                }
            }
            else
            {
                result = new ApiResponse(HttpStatusCode.Conflict, "Category is already exists");
            }
            return result;
        }

        public async Task<ApiResponse> CreateSubCategoryAndLevels(CreateSubCategoryDto categories)
        {
            var subCategoryLevel =
                await
                    _db.Database.ExecuteSqlCommandAsync(
                        $@"
                            ;WITH cte(type,level ) AS(
                            SELECT  c.name,level=0 FROM Categories c where c.name='{categories
                            .CategoryType}' 
                            UNION ALL
                            SELECT ct.type,level+1 FROM cte ct 
                            where level<1
                            )
                            insert into Categories (name,levels) select '{categories
                                .CategoryType}'+'/'+'{categories.SubCategoryType}',
                            (LEN('{categories
                                    .CategoryType}')-LEN(Replace('{categories.CategoryType}','/',''))+1)  
                            from cte where level=1");
            if (subCategoryLevel == 0)
            {
                return new ApiResponse(HttpStatusCode.NotImplemented, "sub category does not insert");
            }

            return new ApiResponse(HttpStatusCode.OK, "Success", subCategoryLevel);
        }
    }
}