using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DataLayer.Constant;
using DataLayer.Context;
using DataLayer.Models;
using Repository.Dtos;
using Repository.Interfaces;

namespace Repository.Implementations
{
    public class PostRepository : IPostRepository
    {

        private readonly WheretogetContext _db;
        private readonly ResourceRepository _resource;
        private readonly UserRepository _user;
        private readonly PostCategoryRepository _postCategory;

        public PostRepository(WheretogetContext db)
        {
            _db = db;
            _resource=new ResourceRepository(db);
            _user = new UserRepository(db);
            _postCategory = new PostCategoryRepository(db);
        }

        public async Task<ApiResponse> CreatePost(Posts newPost)
        {
            ApiResponse result;

            var user = await _db.User.Where(r => r.UserId == newPost.UserId).FirstOrDefaultAsync();
            if(user != null)
            {
                Posts post = new Posts()
                {
                    Id=Guid.NewGuid(),
                    UserId = user.UserId,
                    Title = newPost.Title,
                    ResourceId=newPost.ResourceId,
                    Description = newPost.Description,
                    CreateDate = newPost.CreateDate,
                    UpdateDate = newPost.UpdateDate

                };
                _db.Entry(post).State = EntityState.Added;

                try
                {
                    await _db.SaveChangesAsync();
                    result= new ApiResponse(System.Net.HttpStatusCode.Accepted, "Create post successfully", post);
                }
                catch (Exception ex)
                {
                    result =new ApiResponse(System.Net.HttpStatusCode.NotModified, ex.Message, post);
                }
            }
            else
            {
                result =new ApiResponse(System.Net.HttpStatusCode.NotFound, "User not found");
            }

            return result;
        }
        public async Task<ApiResponse> CreatePostAndReesource(CreatePostDto postAttributes)
        {
            ApiResponse result;

            bool isResourceExists = await _db.Resource.AnyAsync(r => r.Path == postAttributes.Path);

            if (!isResourceExists)
            {
                Resources resource = new Resources()
                {
                    Path = postAttributes.Path,
                    Name = postAttributes.Name,
                    MimeType = postAttributes.MimeType
                };

                await _resource.CreateResource(resource);
                result = new ApiResponse(System.Net.HttpStatusCode.Accepted, "Resource create by successfull", resource);

                bool isPostResourceExists = await _db.Post.AnyAsync(r => r.Resource.Path == resource.Path);

                if(!isPostResourceExists)
                {
                    var user = await _user.GetUser(postAttributes.AuthorId);

                    var currentResource = await _resource.GetResources(resource.Path);

                    if (user != null)
                    {
                        Posts post = new Posts()
                        {
                            UserId = user.UserId,
                            ResourceId= currentResource.Id,
                            Title = postAttributes.Title,
                            Description = postAttributes.Description
                        };

                        await CreatePost(post);
                        await _postCategory.CreatePostCategory(post.Id,postAttributes.Name);

                        result = new ApiResponse(System.Net.HttpStatusCode.Accepted, "Post Record Create successfull", post);
                        
                    }

                    result = new ApiResponse(System.Net.HttpStatusCode.NotFound, "User not Found");
                }

                result = new ApiResponse(System.Net.HttpStatusCode.NotImplemented, "Post Record is already exists");
            }

            result = new ApiResponse(System.Net.HttpStatusCode.NotImplemented, "Record is already exists");

            return result;
        }

    }
}