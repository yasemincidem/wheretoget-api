﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DataLayer.Constant;
using DataLayer.Context;
using Repository.Dtos;
using Repository.Interfaces;
using DataLayer.Models;

namespace Repository.Implementations
{
    public class CommentRepository:ICommentRepository
    {
        private readonly WheretogetContext _db;

        public CommentRepository(WheretogetContext db)
        {
            this._db = db;
        }

        public async Task<ApiResponse> CreateComment(CreateCommentDto newComment)
        {

            ApiResponse result;

            bool isUserExists = await _db.User.AnyAsync(r => r.UserId == newComment.AuthorId);
            Posts post = await _db.Post.Where(r => r.Id == newComment.PostId).FirstOrDefaultAsync();

            if (isUserExists && post != null)
            {

                var comment = new Comment()
                {
                    Id = Guid.NewGuid(),
                    AuthorId = newComment.AuthorId,
                    Text = newComment.Text,
                    Post = post,
                    CreateDate = newComment.CreateDate,
                    UpdateDate = newComment.UpdateDate
                };

                _db.Entry(comment).State = EntityState.Added;

                try
                {

                    await _db.SaveChangesAsync();
                    result = new ApiResponse(HttpStatusCode.Accepted, "Comment created successfully", comment);

                }
                catch (Exception ex)
                {
                    result = new ApiResponse(HttpStatusCode.NotModified, ex.Message, comment);
                }

            }
            else
            {
                result=new ApiResponse(HttpStatusCode.NotFound,"User and post not found");
            }

            return result;
        }

    }
}
