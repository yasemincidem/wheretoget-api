﻿

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DataLayer.Constant;
using DataLayer.Context;
using Repository.Dtos;
using Repository.Interfaces;
using DataLayer.Models;

namespace Repository.Implementations
{
    public class TipRepository : ITipRepository
    {
        private readonly WheretogetContext _db;

        public TipRepository(WheretogetContext db)
        {
            this._db = db;
        }

        public async Task<Tips>  GetTip()
        {
            var tip = await _db.Tips.OrderByDescending(r => r.CreateDate).FirstOrDefaultAsync();

            return tip;
        }
        public async Task<ApiResponse> CreateTip(CreateTipDto newTip)
        {
            ApiResponse result;

            bool userIsExists = await _db.User.AnyAsync(r => r.UserId == newTip.AuthorId);
            bool postIsExists = await _db.Post.AnyAsync(r => r.Id == newTip.PostId);

            if (userIsExists && postIsExists)
            {
                var tip = new Tips()
                {
                    Description = newTip.Description,
                    Url = newTip.Url,
                    Price = newTip.Price,
                    AuthorId = newTip.AuthorId,
                    PostId = newTip.PostId,
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now
                };
                _db.Entry(tip).State = EntityState.Added;

                try
                {
                    await _db.SaveChangesAsync();
                    result= new ApiResponse(HttpStatusCode.Accepted, "Created tip successfully", tip);

                }
                catch (Exception ex)
                {
                    result= new ApiResponse(HttpStatusCode.NotImplemented, ex.Message, tip);
                }
            }

            else
            {
                result=  new ApiResponse(HttpStatusCode.NotFound,"User or post not found");
            }

            return result;
        }
    }
}
