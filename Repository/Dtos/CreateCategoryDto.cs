﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Dtos
{
    public class CreateCategoryDto:BaseClassDto
    {
        public string Name { get; set; }
        public int Levels { get; set; }
    }
}
