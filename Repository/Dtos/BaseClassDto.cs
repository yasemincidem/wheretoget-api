﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Dtos
{
	public class BaseClassDto
	{
		public DateTime? CreateDate { get; set; }
		public DateTime? UpdateDate { get; set; }
	}
}
