﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Dtos
{
    public class CreateSubCategoryDto:BaseClassDto
    {
        public string CategoryType { get; set; }
        public string SubCategoryType { get; set; }
    }
}
