﻿using System;
using System.Collections.Generic;


namespace Repository.Dtos
{
	public class CreatePostDto:BaseClassDto
	{
		public string Description { get; set; }
		public string  Title { get; set; }
		public Guid AuthorId { get; set; }
        public string Comment { get; set; }
        public string Path { get; set; }
        public string Name { get; set; }
        public string MimeType { get; set; }
        public string Category { get; set; }
	}
}
