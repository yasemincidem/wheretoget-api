﻿using System;


namespace Repository.Dtos
{
	public class CreateCommentDto:BaseClassDto
	{
		public Guid AuthorId { get; set; }
		public string Text { get; set; }
		public Guid PostId { get; set; }
	}
}
