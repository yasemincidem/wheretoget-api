﻿using System;

namespace Repository.Dtos
{
    public class CreateAndUpdateUserDto:BaseClassDto
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int? Likes { get; set; }
        public int Gender { get; set; }
        public string Location { get; set; }
        public string Avatar { get; set; }
    }
}
