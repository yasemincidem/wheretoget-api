﻿using Autofac;
using DataLayer.Context;
using Repository.Implementations;
using Repository.Interfaces;

namespace Repository
{
    public class RepositoryModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.Register(c => new UserRepository(new WheretogetContext()))
                .As<IUserRepository>()
                .InstancePerLifetimeScope();
            builder.Register(c => new PostRepository(new WheretogetContext()))
               .As<IPostRepository>()
               .InstancePerLifetimeScope();
            builder.Register(c => new CategoryRepository(new WheretogetContext()))
               .As<ICategoryRepository>()
               .InstancePerLifetimeScope();
            builder.Register(c => new CommentRepository(new WheretogetContext()))
               .As<ICommentRepository>()
               .InstancePerLifetimeScope();
            builder.Register(c => new TipRepository(new WheretogetContext()))
             .As<ITipRepository>()
             .InstancePerLifetimeScope();
        }
    }
}
