﻿

using System;
using System.Threading.Tasks;
using DataLayer.Constant;
using DataLayer.Models;

namespace Repository.Interfaces
{
    interface IPostCategoryRepository
    {
        Task<ApiResponse> CreatePostCategory(Guid postId, string categoryName);

    }
}
