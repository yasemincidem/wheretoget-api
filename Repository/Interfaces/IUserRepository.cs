﻿using DataLayer.Constant;
using Repository.Dtos;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface IUserRepository
    {
         Task<ApiResponse> CreateUser(CreateAndUpdateUserDto u);
         Task<ApiResponse> UpdateUser(CreateAndUpdateUserDto u);
         Task<ApiResponse> DeleteUser(string emailAddress);
		 Task<bool> CheckUserIsExists(string email,string password);
	}
}
