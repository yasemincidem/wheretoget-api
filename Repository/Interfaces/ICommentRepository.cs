﻿using System.Threading.Tasks;
using DataLayer.Constant;
using Repository.Dtos;

namespace Repository.Interfaces
{
    public interface ICommentRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newComment">The comment which will be saved.</param>
        /// <returns></returns>
        Task<ApiResponse> CreateComment(CreateCommentDto newComment);
    }
}