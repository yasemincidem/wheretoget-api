﻿
using DataLayer.Constant;
using DataLayer.Models;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    interface IResourceRepository
    {
        Task<Resources> GetResources(string path);
        Task<ApiResponse> CreateResource(Resources newResource);
    }
}
