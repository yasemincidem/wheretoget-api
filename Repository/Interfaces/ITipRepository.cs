﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DataLayer.Constant;
using DataLayer.Models;
using Repository.Dtos;

namespace Repository.Interfaces
{
    public interface ITipRepository
    {
        Task<Tips> GetTip();

        Task<ApiResponse> CreateTip(CreateTipDto newTip);

    }
}
