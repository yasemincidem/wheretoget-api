using System.Threading.Tasks;
using DataLayer.Constant;
using Repository.Dtos;
using DataLayer.Models;

namespace Repository.Interfaces
{
    public interface IPostRepository
    {
        Task<ApiResponse> CreatePost(Posts newPost);
        Task<ApiResponse> CreatePostAndReesource(CreatePostDto postAttributes);
    }
}