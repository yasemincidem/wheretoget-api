﻿using System.Threading.Tasks;
using DataLayer.Constant;
using Repository.Dtos;

namespace Repository.Interfaces
{
    public interface ICategoryRepository
    {
        Task<ApiResponse> CreateCategory(CreateCategoryDto c);
        Task<ApiResponse> CreateSubCategoryAndLevels(CreateSubCategoryDto categories);
    }
}