﻿using Autofac;
using DataLayer.Context;

namespace DataLayer
{
    public class DataLayerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<IWheretogetContext>().As<WheretogetContext>().SingleInstance();
        }
    }
}