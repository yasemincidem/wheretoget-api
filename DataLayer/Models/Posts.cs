﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Models
{
    public class Posts
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid ResourceId { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string Title { get; set; }
		public int? Likes { get; set; }
		public Guid UserId { get; set; }
		[ForeignKey("UserId")]
        public Users Author { get; set; }
		[ForeignKey("ResourceId")]
		public virtual Resources Resource { get; set; }
		public virtual ICollection<PostCategories> PostCategories { get; set; }
		public virtual ICollection<Comment> Comments { get; set; }
		public virtual ICollection<Tips> Tips { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
