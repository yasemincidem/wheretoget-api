﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Models
{
    public class Tips
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string Url { get; set; }
        public float Price { get; set; }
        [MaxLength(512)]
        public string Description { get; set; }
		public int? Likes { get; set; }

		public Guid AuthorId { get; set; }
        public Guid? PostId { get; set; }
		[ForeignKey("AuthorId")]
		public Users Author { get; set; }
        [ForeignKey("PostId")]
        public virtual Posts Post { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

    }
}
