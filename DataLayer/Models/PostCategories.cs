﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Models
{
    public class PostCategories
    {
        [Key, Column(Order = 0)]
        public Guid PostId { get; set; }
        [Key, Column(Order = 1)]
        public Guid CategoryId { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public virtual Posts Post { get; set; }
        public virtual Categories Category { get; set; }
    }
}
