﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Models
{
    public class Comment
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        [MaxLength(512)]
        public string Text { get; set; }
		public int? Likes { get; set; }
		public Guid AuthorId { get; set; }

		[ForeignKey("AuthorId")]
        public Users Author { get; set; }
		public virtual Posts Post { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
