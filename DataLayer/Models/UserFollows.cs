﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.Models
{
    public class UserFollows
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Follower { get; set; }
        [Required]
        public string Followed { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
