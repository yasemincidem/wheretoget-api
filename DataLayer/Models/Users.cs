﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Models
{
    public class Users
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UserId { get; set; }
        [Required]
        [MaxLength(50)]
        [RegularExpression(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$", ErrorMessage = "Adresse mail non valide !")]
        public string EmailAddress { get; set; }
        [Required]
        [MaxLength(50)]
        public string Username { get; set; }
        [Required]
        [MaxLength(50)]
        public string Password { get; set; }
        public string Avatar{ get; set; }
        public string Location { get; set; }
		public int? Likes { get; set; }

		public Constant.Enums.GenderEnum? Gender { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public virtual ICollection<UserFollows> UserFolloweds  { get; set; }
        public virtual ICollection<Categories> UserFollowedCategories { get; set; }
        public virtual ICollection<Tips> Tips { get; set; }
		public virtual ICollection<Comment> Comments{ get; set; }
    }
   
}
