﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Models
{
	public class Resources
	{
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Path { get; set; }
        public string Name { get; set; }
        public string MimeType { get; set; }

        public DateTime? CreateDate { get; set; }
		public DateTime? UpdateDate { get; set; }
	}
}
