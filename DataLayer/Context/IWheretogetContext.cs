﻿using System.Data.Entity;
using DataLayer.Models;

namespace DataLayer.Context
{
    public interface IWheretogetContext
    {
        DbSet<Users> User { get; set; }
        DbSet<Categories> Catgegory { get; set; }
        DbSet<Posts> Post { get; set; }
        DbSet<Tips> Tip { get; set; }
        DbSet<Resources> Resource { get; set; }
        DbSet<PostCategories> PostCategory { get; set; }
    }
}