﻿using System.Data.Entity;
using DataLayer.Models;

namespace DataLayer.Context
{
    public class WheretogetContext : DbContext
    {

        public WheretogetContext() : base("name=WheretogetContext")
        {
            //override method for migration
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<WheretogetContext, DataLayer.Migrations.Configuration>("WheretogetContext"));

        }
        public DbSet<Users> User { get; set; }
        public DbSet<Categories> Catgegory { get; set; }
        public DbSet<Posts> Post { get; set; }
        public DbSet<Tips> Tips { get; set; }
        public DbSet<Resources> Resource { get; set; }
        DbSet<PostCategories> PostCategory { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           // modelBuilder.Entity<Tips>()
           //.HasRequired(c => c.Post)
           //.WithMany()
           //.WillCascadeOnDelete(false);

            base.OnModelCreating(modelBuilder);
        }

    }
}
