﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Constant
{
    public class ApiResponse
    {
        public int StatusCode { get; set; }
        public string ErrorMessage { get; set; }
        public object  Response { get; set; }
        public ApiResponse(HttpStatusCode statusCode,string errorMessage=null,object response=null)
        {
            StatusCode = (int)statusCode;
            ErrorMessage = errorMessage;
            Response = response;
        }
    }
   
}
