﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Constant
{
    public class Enums
    {
        public enum GenderEnum
        {
            male = 1,
            female = 2,
            unknown = 3
        }
    }
}
